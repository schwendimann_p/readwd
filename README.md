
# WaveDREAM Binary Data Analysis Tool

## Purpose
This code is designed to analyse the binary data files obtained
from WaveDREAM or DRS4 evaluation boards. It reads the waveforms
for each channel individually, estimates the amplitude and computes
time and integrated charge for signal waveforms. Optionally, a
sinusoidal noise with a known frequency (but unknown phase and
amplitude) can be subtracted prior to the analysis.

The details of the analysis depend on many configuration parameters
that can be defined by the user. The output of the analysis is
written to a TTree saved in a ROOT file. Input configuration and
output are described in more detail below under *Documentation*.


## Installation

To compile this code, you require the root packages downloadable from the CERN website
[https://root.cern.ch/downloading-root](https://root.cern.ch/downloading-root)
and the curses library, which should be installed by default. If not, they can be installed
with
```sudo apt-get install ncurses```


## Usage

Use to read data from binary WaveDREAM output files to root files

```readWD [args] [-o {outputPath}] file1.dat [file2.dat ... fileN.dat]```

Reads the files ```file1.dat ... fileN.dat``` and creates a root file for each

```
-o {outputPath}        set the path where the rootfiles are create default is ./
```

Possible [args] are : 
```
-d, --DEBUG            print debug information
-f, --ForceOverwrite   enforce the overwriting of existing rootfiles
-h, --help             print this help text and quit
-p, --pos\_wf           use for positive waveforms
-c, --config           enter configuration interface, requires user input
-s, --subtractNoise    subtract sine noise from waveforms
```

## Documentation
### Configuration Window
* **nSamples for Integration Windows** Number of samples to be summed up to estimate optimal integration windows for later charge extraction.

* **Number of Events to save Waveforms** A number of waveforms to be saved to the root file.

* **Waveform type** negative waveform is looking for a signal below the pedestal, positive for above.

* **CF Threshold** Constant fraction time extraction threshold. The time is estimated at the point the signal passes this fraction of the maximal amplitude.

* **LE Threshold** Leading Edge threshold. The time is estimated at the point this voltage is crossed for the first time.

* **Integration rise (stdv)** Models the front of the waveform with a Gaussian. This many standard deviations from the sample peak are considered for the integration window.

* **Integration decay (tau)** Models the falling side of the waveform with an exponential decay with decay constant tau. This many decay constants are considered after the peak of the sample for the integration.

* **Sine Noise Subtraction** Enable this if there is a dominant sine noise in your waveforms.

* **Sine Noise Frequency (MHZ)** Frequency of the sine noise to be subtracted.

* **Debug Mode** Allows extra output if needed. Beware, the amount can be quite large in debug mode 2.

### ROOT TTree Branches
* **runnumber** the run number as extracted from the file name
* **event** the event number 
* **timestamp** the timestamp w.r.t. 00:00 of the first day of DAQ

* **time** Constant fraction time
* **timeLE** Leading edge time
* **amplitude** maximum amplitude with respect to the estimated pedestal
* **area** charge integral in integration windows, not corrected for different bin widths (kept for legacy reasons)
* **area2** charge integral in integration windows corrected for time bin width.
* **ped** pedestal level
* **Stdv** standard deviation (noise level) for the pedestal
* **StdvRaw** standard deviation for the pedestal before sine noise subtraction.
* **SineAmplitude** Amplitude of the subtracted sine noise
* **SinePhase** Phase of the subtracted sine noise
* **TrigCell** Trigger Cell of the DRS4 chip, i.e. the cell where the chip was stopped.

## Contact
Patrick Schwendimann <patrick.schwendi@gmail.com>

## Acknowledgement
This code is based on the old DRS4 binary data analysis program orignially
written for the ATAR experiment by Angela Papa <angela.papa@psi.ch>, later
updated by Emanuele Ripiccini, Fred Gray and Giada Rutar and an example
program written by Stefan Ritt <stefan.ritt@psi.ch> for the reading of a
binary file written by the DRSOsc program.

## Disclaimer
This code may not be fully suitable for whatever purpose you may intend to use it for.
No warranty is given. Use it always together with some common sense.
